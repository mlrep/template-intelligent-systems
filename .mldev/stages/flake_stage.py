# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import os

from mldev.experiment import experiment_tag
from mldev.experiment import BasicStage

#@experiment_tag
#this code is broken - Stage definition changed, do not override file versioning
class Flake(BasicStage):

    def __init__(self, attributes={}, versioned_input_data={}, versioned_output_data={}, script=[]):
        super().__init__()

        self.update(attributes, versioned_input_data, versioned_output_data, script)

    def update(self, attributes, versioned_input_data, versioned_output_data, script):
        self.attributes = attributes
        self.versioned_input_data = versioned_input_data
        self.versioned_output_data = versioned_output_data
        self.script = script

    def to_dict(self):
        return dict(
            attributes=self.attributes,
            versioned_input_data=self.versioned_input_data,
            versioned_output_data=self.versioned_output_data,
        )

    def __repr__(self):
        return (
            "{}(attributes={}, versioned_input_data={}, versioned_output_data={})"
            .format(
                self.__class__.__name__,
                self.attributes,
                self.versioned_input_data,
                self.versioned_output_data,
            )
        )

    def __call__(self, *args, **kwargs):
        print(self.script)
        stage_name = args[0]
        if stage_name:
            if not self.prepared and self.attributes.get("needs_dvc", "True") == "True":
                self.prepare(stage_name)
                if self.attributes.get("monitoring"):
                    self.run_monitoring_services("pre_run")
                os.system("dvc push -v")
            if self.prepared and self.attributes.get("needs_dvc", "True") == "True":
                self.run(stage_name)
                self.run_monitoring_services("run")
                os.system("dvc push -v")
            self.prepared = True