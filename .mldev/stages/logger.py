from collections.abc import Mapping
from functools import wraps

from torch.utils.tensorboard import SummaryWriter
import os

def get_tensorboard_logger_env(env: Mapping):
    log_dir = env.get('MLDEV_LOG_DIR', './logs/')
    run_name = env.get('MLDEV_RUN_NAME', '')
    run_index = env.get('MLDEV_RUN_INDEX', '')

    return get_tensorboard_logger(log_dir=log_dir, run_name=run_name, run_index=run_index)


def wrap_logger(m, run_name):
    @wraps(m)
    def wrapper(tag, *args, **kwargs):
        new_tag = tag if str(tag).startswith("/") else f"{run_name}/{tag}"
        return m(new_tag, *args, **kwargs)

    return wrapper


def get_tensorboard_logger(log_dir, run_name, run_index):
    logger = SummaryWriter(os.path.join(str(log_dir), str(run_name), str(run_index)),
                           flush_secs=1)

    for member in dir(logger):
        if member.startswith('add_'):
            method = getattr(logger, member)
            if callable(method):
                setattr(logger, member, wrap_logger(method, run_name))

    logger.run_name = run_name
    logger.run_index = run_index
    logger.log_dir = log_dir

    return logger
