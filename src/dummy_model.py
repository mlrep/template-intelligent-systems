# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

from torch import nn


class DummyNet(nn.Module):
    def __init__(self, dim_in, dim_hidden1, dim_out):
        """
        :param dim_in: input dimension;
        :param dim_hidden1: hidden dimension;
        :param dim_out: output dimension;
        """
        super(DummyNet, self).__init__()
        self.linear1 = nn.Linear(dim_in, dim_hidden1)
        self.relu1 = nn.ReLU()
        self.linear2 = nn.Linear(dim_hidden1, dim_out)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.linear1(x)
        x = self.relu1(x)
        x = self.linear2(x)
        x = self.softmax(x)
        return x
